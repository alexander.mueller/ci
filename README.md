Docker images for GitLab CI
===========================

Images
------

The current images are provided in the registry, `registry.dune-project.org/docker/ci/...`,
and are mirrored in the docker hub, `duneci/...`:

### Base Images

The base images all offer a set of _toolchains_. You can select a toolchain by defining the
job variable `DUNECI_TOOLCHAIN`. The corresponding opts file is always `/duneci/dune.opts`,
but that will be automatically picked up by the scripts in the image. Toolchains are given
by strings of the following format: `<compiler>-<version>-<optional extras>-<c++ standard>`.

| image                      | description                        | toolchains |
|----------------------------|-----------------------------------|------------|
| [debian:9](registry.dune-project.org/docker/ci/debian:9)      | Debian 9 with cmake 3.7   | gcc-6-14, gcc-6-noassert-14, clang-3.8-14, clang-3.8-noassert-14 |
| [debian:10](registry.dune-project.org/docker/ci/debian:10)    | Debian 10 with cmake 3.11 | gcc-7-14, gcc-7-noassert-14, gcc-7-17, gcc-8-17, gcc-8-noassert-17, clang-6-17, clang-6-noassert-17, clang-7-libcpp-17, clang-7-libcpp-noassert-17 |
| [debian:11](registry.dune-project.org/docker/ci/debian:11)    | Debian 11 with cmake 3.16 | gcc-9-17, gcc-9-20, gcc-9-noassert-20, clang-8-libcpp-17, clang-8-libcpp-noassert-17 |
| [ubuntu:18.04](registry.dune-project.org/docker/ci/ubuntu:18.04) | Ubuntu LTS 18.04 with updated cmake 3.17 | gcc-7-14, gcc-7-noassert-14, gcc-7-17, gcc-7-noassert-17, clang-6-17, clang-6-noassert-17 |
| [ubuntu:20.04](registry.dune-project.org/docker/ci/ubuntu:20.04) | Ubuntu LTS 20.04 with cmake 3.16         | gcc-9-17, gcc-9-20, gcc-9-noassert-17, gcc-9-noassert-20, clang-10-17, clang-10-20, clang-10-noassert-17, clang-10-noassert-20 |

### Core Images

These images contain the core and staging modules compiled for several combinations of distributions and toolchains. The toolchain is locked
in for these images. See https://gitlab.dune-project.org/docker/ci/container_registry/46 for a list of possible setups.

There is also some default configurations for each version:

| image                  | distribution | toolchain |
|------------------------|--------------|-----------|
| [dune:2.5](registry.dune-project.org/docker/ci/dune:2.5) | debian-9     | gcc-6-14  |
| [dune:2.6](registry.dune-project.org/docker/ci/dune:2.6) | debian-10    | gcc-7-14  |
| [dune:2.7](registry.dune-project.org/docker/ci/dune:2.7) | debian-10    | gcc-8-17  |
| [dune:2.8](registry.dune-project.org/docker/ci/dune:2.8) | debian-11    | gcc-9-20  |
| [dune:2.9](registry.dune-project.org/docker/ci/dune:git) | debian-11    | gcc-10-20  |
| [dune:git](registry.dune-project.org/docker/ci/dune:git) | debian-11    | gcc-10-20  |


### Module Dependency Images

These images contain all dependencies for testing the respective downstream module. They are mirrored in a similar way to the images listed above.
Find the list of these images in https://gitlab.dune-project.org/docker/ci/container_registry/47.


### Module Images

These images are for testing downstream modules: https://gitlab.dune-project.org/docker/ci/container_registry/49

`.gitlab-ci.yml`
----------------

Installing dependencies:
```yaml
before_script:
  - duneci-install-module https://gitlab.dune-project.org/core/dune-common.git
  - duneci-install-module https://gitlab.dune-project.org/core/dune-geometry.git
```

To build with several images:
```yaml
---
dune:2.4--gcc:
  image: registry.dune-project.org/docker/ci/dune:2.6
  script: duneci-standard-test

dune:2.4--clang:
  image: registry.dune-project.org/docker/ci/dune:2.6
  script: duneci-standard-test --opts=/duneci/opts.clang
```

You can also specify a default image and use it in several jobs:

```yaml
---
image: registry.dune-project.org/docker/ci/dune:2.6

dune:2.4--gcc:
  script: duneci-standard-test

dune:2.4--clang:
  script: duneci-standard-test --opts=/duneci/opts.clang
```

For more information, take a look at the [GitLab documentation on `.gitlab-ci.yml`](https://docs.gitlab.com/ce/ci/yaml/README.html).

Running the pipelines
---------------------

In order to manually trigger a pipeline one can specify which version to build: `2.5`, `2.6`, `2.7`, `2.8`, `2.9`, or `git`.
This is set by the variable `TRIGGER_VERSION`.

Adding new images
-----------------

In order to add a new image, create a directory with the Dockerfile and any additional files that you want to have available in the Docker
build context. The naming scheme of the directory has to be `<image name>-<tag>`. Then edit `.gitlab-ci.yml` and add an appropriate entry to
the list of images in the correct stage (base / core / modules). If you want to have the image mirrored to [Docker Hub](https://hub.docker.com/),
talk to Ansgar or Steffen; they have to create the repository on the hub before that works.
