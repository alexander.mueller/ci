#! /bin/bash

set -e
set -u

set -- --opts="/duneci/dune.opts" "${@}"

DUNECONTROL=dunecontrol
if [[ -x bin/dunecontrol ]]; then
  DUNECONTROL=bin/dunecontrol
fi

DUNE_CTEST=duneci-ctest
if [[ -x bin/dune-ctest ]]; then
  DUNE_CTEST=../bin/dune-ctest
fi

parallel_opts=
if [[ -v DUNECI_PARALLEL ]]; then
  echo "Parallel run with ${DUNECI_PARALLEL} processes"
  parallel_opts="-j${DUNECI_PARALLEL}"
fi

assert_label_valid()
{
  if [[ $1 != +([[:word:]-]) ]]; then
    cat >&2 <<EOF
duneci-standard-test: Error: Invalid label in DUNECI_TEST_LABELS: '$label'
duneci-standard-test: Note: Only alphanumeric characters plus '_' and '-' are
duneci-standard-test: Note: allowed in label names, and the names must be
duneci-standard-test: Note: non-empty.
EOF
    exit 2
  fi
}

# Parse labels in $1 and set ${parsed_targets[@]} and ${parsed_selectors[@]}
# accordingly.  Labels in $1 may be seperated by whitespace or ','.
parse_labels()
{
  # use a function to automatically restore IFS
  local IFS="$IFS," label regexs status=1

  parsed_targets=()
  regexs=()
  for label in $1; do
    # ensure the labels can be safely handled
    assert_label_valid "$label"

    parsed_targets+=("build_${label}_tests")
    regexs+=("^${label}\$")

    status=0 # we found at least one label
  done

  parsed_selectors=()
  if [[ ${#regexs[@]} -gt 0 ]]; then
    # This will join all regexps using '|' from IFS
    IFS="|"
    parsed_selectors=(-L "${regexs[*]}")
  fi

  return $status
}

build_test_targets=(build_tests) # passed to make/ninja to build tests
select_test_args=()              # passed to ctest to select tests
if parse_labels "${DUNECI_TEST_LABELS-}"; then
    build_test_targets=("${parsed_targets[@]}")
    select_test_args=("${parsed_selectors[@]}")
fi


# Allow oversubscription (tests might want to try having more ranks
# than environment has processors) and force degraded mode (as we
# might run multiple tests in parallel and this is not good with
# OpenMPI's agressive mode)
# References:
#  - https://bugs.debian.org/850229
#  - https://gitlab.dune-project.org/core/dune-grid/issues/67
#  - https://www.open-mpi.org/faq/?category=running#oversubscribing
export OMPI_MCA_rmaps_base_oversubscribe=1
export OMPI_MCA_mpi_yield_when_idle=1

# Shut up OpenMPI warning about not being able to use the InfiniBand
# interface on the Heidelberg nodes, it's not needed anyway
export OMPI_MCA_btl_base_warn_component_unused=0

set -x
${DUNECONTROL} --current "${@}" vcsetup
${DUNECONTROL} --current "${@}" configure

if [[ -v DUNECI_CONFIGURE_ONLY ]]; then
    exit
fi

# Always try to do all test stages - dune-ctest will trigger on missing tests,
# and we manually catch installation errors
# We also have to pass "-k" to make to have it keep going as much as possible
# This won't work for ninja, which required "-k 0"
set +e
build_failed=
${DUNECONTROL} --current "${@}" make -k ${parallel_opts} all || build_failed=1
if [[ "${DUNECI_TEST_INSTALL:-0}" != 0 ]] ; then
    ${DUNECONTROL} --current "${@}" make -k ${parallel_opts} install
    install_result=$?
    export DUNECI_INSTALL_RESULT=${install_result}
fi
${DUNECONTROL} --current "${@}" make -k ${parallel_opts} "${build_test_targets[@]}" || build_failed=1
# note the extra quoting for select_test_args to protect it from dunecontrol evaling it
if ${DUNECONTROL} --current "${@}" bexec ${DUNE_CTEST} ${parallel_opts} ${select_test_args[@]:+"${select_test_args[@]@Q}"} ; then
    if [[ -v install_result && ${install_result} -ne 0 ]] ; then
        echo "Error: The installation test failed"
        exit 1
    elif [[ -n "${build_failed}" ]]; then
        echo >&2 "ERROR: Build failed"
        exit 1
    else
        exit 0
    fi
else
    exit 1
fi
