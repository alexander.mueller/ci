#! /bin/bash
# Author: Ansgar Burchardt <Ansgar.Burchardt@tu-dresden.de>

set -e
set -u

usage() {
    echo "usage: duneci-install-module [options] <Git-repository> <dunecontrol-opts>" >&2
    echo >&2
    echo "options:" >&2
    echo "  -b <branch>, --branch <branch>: Install branch <branch> of the module." >&2
    echo "  -r, --recursive: Clone the git repository with --recursive (for git submodules)." >&2
    echo >&2
    echo "environment variables:" >&2
    echo "  DUNECI_BRANCH:" >&2
    echo "      branch to use if none is specified via -b|--branch;" >&2
    echo "      empty for default branch (usually 'master'); default: ''" >&2
    echo "  DUNECI_BRANCH_AUTO:" >&2
    echo "      if '1', try to use the same branch as build by the CI system," >&2
    echo "      that is \${CI_COMMIT_REF_NAME};  default: '1'" >&2
    echo "  DUNECI_INSTALL_STAGE:" >&2
    echo "      if '1', perform install stage and remove build folder;  default: ''" >&2
    echo "files:" >&2
    echo "  /duneci/dune.opts" >&2
    echo "      DUNE options file used to call 'dunecontrol' script" >&2
    echo "  /duneci/install/log/*.txt" >&2
    echo "      verbose output of all modules installed by this script" >&2
    exit ${1:-0}
}

if [[ $# -eq 0 ]]; then
    usage 0
fi


branch="${DUNECI_BRANCH:-}"
recursive=

while :; do
    case "${1}" in
        -b|--branch)
            branch="${2}"
            shift 2
            ;;
        -r|--recursive)
            recursive=1
            shift 1
            ;;
        *)
            break
            ;;
    esac
done

repo="${1}"
module="${repo##*/}"; module="${module%.git}"
shift

mkdir -p /duneci/install/log
logfile=/duneci/install/log/${module}.txt

cd /duneci/modules
if [[ -d "${module}" || -f  "${logfile}" ]]; then
    echo "Module ${module} is already installed." >&2
    exit 1
fi

git_clone_opts=(--depth=1 --no-single-branch)
if [[ -n "${branch}" ]]; then
    git_clone_opts+=(-b "${branch}")
fi
if [[ -n "${recursive}" ]]; then
    git_clone_opts+=("--recursive")
fi

git clone "${git_clone_opts[@]}" "${repo}"  | tee -a ${logfile}
if [[ "${DUNECI_BRANCH_AUTO:-1}" = "1" && -n "${CI_COMMIT_REF_NAME:-}" && "${CI_COMMIT_REF_NAME:-}" != "${CI_DEFAULT_BRANCH:-master}" ]]; then
    # It is not an error if ${CI_COMMIT_REF_NAME} is not a branch
    # in the module we are about to install.
    git -C "${module}" checkout "${CI_COMMIT_REF_NAME}" || :
fi

if [[ "${module}" = dune-common ]]; then
    for f in dunecontrol dune-ctest; do
        src=/duneci/modules/dune-common/bin/${f}
        if [[ -x ${src} ]]; then
            ln -sf ${src} /duneci/bin/
        fi
    done
fi

opts="/duneci/dune.opts"

if [[ -v DUNECI_PARALLEL ]]; then
    echo "Parallel run with ${DUNECI_PARALLEL} processes" | tee -a ${logfile}
    set -- --make-opts="-j${DUNECI_PARALLEL}" "${@}"
fi

(
    cd "${module}"
    git_branch=$(git symbolic-ref HEAD 2>/dev/null || echo "(unknown)")
    git_branch="${git_branch#refs/heads/}"
    echo                                                            | tee -a ${logfile}
    echo "Installing ${module}"                                     | tee -a ${logfile}
    echo "  url:    ${repo}"                                        | tee -a ${logfile}
    echo "  branch: ${git_branch}"                                  | tee -a ${logfile}
    echo "  commit: $(git rev-parse HEAD)"                          | tee -a ${logfile}
    echo "  date:   $(git log -1 --format='tformat:%ci') (commit)"  | tee -a ${logfile}
    echo "  using opts file ${opts}"                                | tee -a ${logfile}
    echo                                                            | tee -a ${logfile}
)

dunecontrol --opts="${opts}" "${@}" --only="${module}" all 2>&1     | tee -a ${logfile}

if [[ "${DUNECI_INSTALL_STAGE:-}" = "1" ]]; then
    cd /duneci/modules
    { # try
        cmake --build /duneci/modules/${module}/build-cmake --target install | tee -a ${logfile}
        rm -rf "${module}"
    } || { # catch
        echo "Module ${module} failed on installation stage." >&2
        echo "" >&2
        echo "  * If the module should not be installed, unset dune CI installation stage" >&2
        echo "" >&2
        echo "        variables: " >&2
        echo "          DUNECI_INSTALL_STAGE:''" >&2
        echo "" >&2
        echo "  * If the module should be installable but you need the pipeline to temporarily pass, allow failures with error code 65" >&2
        echo "" >&2
        echo "        allow_failure:"  >&2
        echo "          exit_codes:" >&2
        echo "            - 65" >&2
        echo "" >&2
        exit 65
    }
else
    find "${module}/build-cmake" -name CMakeFiles -exec rm -rf -- "{}" +
fi
